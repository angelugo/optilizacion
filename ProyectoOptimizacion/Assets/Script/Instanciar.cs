using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class Instanciar : MonoBehaviour
{
    public GameObject objectToSpawn; // Objeto que se generará
    public Transform spawnPoint; // Punto de origen de generación
    ObjectPooling objectPooler;
    private void Start()
    {
         objectPooler= ObjectPooling.instance;
    }

    private void FixedUpdate()
    {
        objectPooler.SpawnPromPool("Objeto", transform.position, Quaternion.identity);
    }
}

