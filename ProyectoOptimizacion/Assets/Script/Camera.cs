using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    public float moveSpeed = 5f; // Velocidad de movimiento de la c�mara

    void Update()
    {
        // Obtener la entrada de teclado
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // Calcular el desplazamiento de la c�mara en funci�n de la entrada de teclado
        Vector3 moveDirection = new Vector3(horizontalInput, 0f, verticalInput) * moveSpeed * Time.deltaTime;

        // Aplicar el desplazamiento a la posici�n de la c�mara
        transform.Translate(moveDirection, Space.Self);
    }
}
