using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawDistance : MonoBehaviour
{
    public Transform player; // La posici�n del jugador
    public float distanceThreshold = 50f; // Umbral de distancia para renderizar objetos

    private void Update()
    {
        // Iterar a trav�s de todos los objetos en la escena
        foreach (GameObject obj in FindObjectsOfType<GameObject>())
        {
            // Verificar si el objeto tiene un renderer
            Renderer renderer = obj.GetComponent<Renderer>();
            if (renderer != null)
            {
                // Calcular la distancia entre el jugador y el objeto
                float distanceToPlayer = Vector3.Distance(obj.transform.position, player.position);

                // Si la distancia es mayor que el umbral, desactivar el renderer
                if (distanceToPlayer > distanceThreshold)
                {
                    renderer.enabled = false;
                }
                else
                {
                    // Si la distancia es menor que el umbral, activar el renderer
                    renderer.enabled = true;
                }
            }
        }
    }
}